# MATOSKI Public License #

## License Summary ##
* License does not expire.
* Can be distributed in 5 projects
* Can be distributed and / or packaged as a code or binary product (sublicensed)
* Commercial use allowed
* Cannot modify source-code for any purpose (cannot create derivative works)
* Attribution to software creator must be made as specified:
    * Email info@matoski.co.uk with Your business details and intent for using the software
* Additional terms:
    * Do not use this software for any illegal activities.

[For more information on the license summary topics](https://bitbucket.org/matoski/public/wiki/public-license-v1.0)

---