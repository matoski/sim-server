// Copyright:: 2017, MATOSKI LTD, All Rights Reserved.

package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"os"
	"os/signal"
	"regexp"
	"syscall"
)

var (
	config *Config
)

func dumpRequest(req *http.Request) error {
	dump, err := httputil.DumpRequest(req, true)
	if err == nil {
		log.Printf("%s", dump)
	}
	return err
}

func serverHandler(w http.ResponseWriter, req *http.Request) {
	if err := dumpRequest(req); err != nil { // Log the incoming HTTP request
		http.Error(w, fmt.Sprint(err), http.StatusInternalServerError)
		return
	}

	// Look-up Response
	id := req.Method + "::" + req.URL.Path
	res := config.Server.Response
	for pattern, value := range config.Resources {
		if matched, err := regexp.MatchString(pattern, id); err != nil {
			log.Printf("invalid response configuration %s: %v", pattern, err)
		} else if matched {
			res = value.Response // Found configured response
			break
		}
	}

	for header, value := range res.Headers {
		w.Header().Set(header, value)
	}
	w.WriteHeader(res.Code)
	if len(res.Body) > 0 { // Write the response body
		if _, err := w.Write([]byte(res.Body)); err != nil {
			log.Printf("failed to send response for %s: %v", id, err)
		}
	}
}

func main() {
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	yml := flag.String("yml", "config.yaml", "yaml file to read config from")
	flag.Parse()

	log.Printf("Parsing yaml at path: %s\n", *yml)
	var err error
	config, err = Parse(*yml)
	if err != nil {
		log.Fatalf("Failed to parse %s: %v", *yml, err)
	}

	go func() {
		http.HandleFunc("/", serverHandler)

		log.Printf(`Starting HTTP handler on ":%d"`, config.Server.Port)
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", config.Server.Port), nil))
	}()

	s := <-c
	fmt.Println("Got signal:", s)
	os.Exit(0)
}
