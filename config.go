package main

import (
	"io/ioutil"

	yaml "gopkg.in/yaml.v2"
)

// Config represents the structure of the yaml file
type Config struct {
	Server    Server              `yaml:"server,omitempty"`
	Resources map[string]Resource `yaml:"resources"`
}

// Server represents the configuration for the server
type Server struct {
	Port     int      `yaml:"port,omitempty"`
	Response Response `yaml:"response,omitempty"`
}

// Resource represents a configured HTTP resource
type Resource struct {
	Response Response `yaml:"response,omitempty"`
}

// Response represents a configured response
type Response struct {
	Code    int               `yaml:"code,omitempty"`
	Headers map[string]string `yaml:"headers,omitempty"`
	Body    string            `yaml:"body,omitempty"`
}

// Parse takes a path to yaml file and produces a parsed Config
func Parse(path string) (*Config, error) {
	var c Config

	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	if err = yaml.Unmarshal(data, &c); err != nil {
		return nil, err
	}

	return &c, err
}
