# Simulation Server

This simulation server can be used in integrated tests to receive, record and simulate responses.

![Architecture Diagram](Architecture_Diagram.png?raw=true "Architecture Diagram")

## Docker Image

The official docker images for [matoski/sim-server](https://hub.docker.com/r/matoski/sim-server/).

## License Terms
This software is published under the [MATOSKI Public License](https://bitbucket.org/matoski/public/wiki/public-license-v1.0). You can use and distribute this software on up to 5 projects and You cannot modify source-code for any purpose (cannot create derivative works).